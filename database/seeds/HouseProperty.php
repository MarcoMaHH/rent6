<?php

use think\migration\Seeder;

class HouseProperty extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $this->table('house_property')->insert([
            ['admin_user_id' => 1, 'name' => '最常用的房屋', 'address' => '地址', 'firstly' => 'Y']
        ])->save();
    }
}
