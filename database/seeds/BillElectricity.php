<?php

use think\migration\Seeder;

class BillElectricity extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $this->table('bill_electricity')->insert([
            ['name' => '最常用的房屋-电费', 'type' => 'B', 'house_property_id' => '1'],
            ['name' => '最常用的房屋-水费', 'type' => 'C', 'house_property_id' => '1']
        ])->save();
    }
}
