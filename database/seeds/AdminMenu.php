<?php

use think\migration\Seeder;

class AdminMenu extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $this->table('admin_menu')->insert([
            ['id' => 1, 'pid' => 0, 'meta_title' => '设置', 'meta_icon' => 'system-setting',
             'path' => '/admin', 'name' => 'admin', 'component' => 'LAYOUT', 'redirect' => '/admin/role',
             'controller' => 'admin', 'sort' => 99],

            ['id' => 2, 'pid' => 1, 'meta_title' => '角色管理', 'meta_icon' => '',
             'path' => 'role', 'name' => 'AdminRole', 'component' => '/admin/role/index', 'redirect' => '',
             'controller' => 'admin.role', 'sort' => 1],

            ['id' => 3, 'pid' => 1, 'meta_title' => '权限管理', 'meta_icon' => '',
            'path' => 'permission', 'name' => 'AdminPermission', 'component' => '/admin/permission/index', 'redirect' => '',
            'controller' => 'admin.permission', 'sort' => 2],

            ['id' => 4, 'pid' => 1, 'meta_title' => '用户管理', 'meta_icon' => '',
            'path' => 'user', 'name' => 'AdminUser', 'component' => '/admin/user/index', 'redirect' => '',
            'controller' => 'admin.user', 'sort' => 3],

             ['id' => 5, 'pid' => 0, 'meta_title' => '房屋管理', 'meta_icon' => 'houses',
             'path' => '/house', 'name' => 'house', 'component' => 'LAYOUT', 'redirect' => '/house/property',
             'controller' => 'house', 'sort' => 1],

            ['id' => 6, 'pid' => 5, 'meta_title' => '房产管理', 'meta_icon' => '',
             'path' => 'property', 'name' => 'HouseProperty', 'component' => '/house/property/index', 'redirect' => '',
             'controller' => 'house', 'sort' => 1],

            ['id' => 7, 'pid' => 5, 'meta_title' => '房间管理', 'meta_icon' => '',
             'path' => 'number', 'name' => 'HouseNumber', 'component' => '/house/number/index', 'redirect' => '',
             'controller' => 'house', 'sort' => 2],

            ['id' => 8, 'pid' => 5, 'meta_title' => '未收账单', 'meta_icon' => '',
             'path' => 'uncollected', 'name' => 'HouseUncollected', 'component' => '/house/uncollected/index', 'redirect' => '',
             'controller' => 'house', 'sort' => 3],

             ['id' => 9, 'pid' => 5, 'meta_title' => '租客信息', 'meta_icon' => '',
             'path' => 'tenant', 'name' => 'HouseTenant', 'component' => '/house/tenant/index', 'redirect' => '',
             'controller' => 'house', 'sort' => 4],
        ])->save();
    }
}
