<?php

use think\migration\Migrator;
use think\migration\db\Column;

class BillSum extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table(
            'bill_sum',
            ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci']
        );
        $table->addColumn(
            'house_property_id',
            'integer',
            ['null' => false, 'default' => 0,  'comment' => '房产id']
        )
        ->addColumn(
            'house_number_id',
            'integer',
            ['comment' => '房号id']
        )
        ->addColumn(
            'type',
            'string',
            ['limit' => 4, 'null' => false, 'default' => '', 'comment' => '类型']
        )
        ->addColumn(
            'total_money',
            'float',
            ['null' => false, 'default' => 0.0, 'comment' => '总金额']
        )
        ->addColumn('accounting_date', 'timestamp', ['null' => true, 'comment' => '到账时间'])
        ->addTimestamps()
        ->create();
    }
}
