<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AdminMenu extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table(
            'admin_menu',
            ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci']
        );
        $table->addColumn(
            'pid',
            'integer',
            ['null' => false, 'default' => 0, 'comment' => '上级id']
        )
        ->addColumn(
            'controller',
            'string',
            ['limit' => 32, 'null' => false, 'default' => '', 'comment' => '控制器']
        )
        ->addColumn(
            'sort',
            'integer',
            ['null' => false, 'default' => 0, 'comment' => '排序值']
        )

        ->addColumn(
            'name',
            'string',
            ['limit' => 32, 'null' => false, 'default' => '', 'comment' => '名称']
        )
        ->addColumn(
            'path',
            'string',
            ['limit' => 32, 'null' => false, 'default' => '', 'comment' => '当前路由的路径']
        )
        ->addColumn(
            'component',
            'string',
            ['limit' => 32, 'null' => false, 'default' => '', 'comment' => '页面组件']
        )
        ->addColumn(
            'redirect',
            'string',
            ['limit' => 32, 'null' => false, 'default' => '', 'comment' => '重定向的路径']
        )
        ->addColumn(
            'meta_title',
            'string',
            ['limit' => 32, 'null' => true, 'default' => '', 'comment' => '展示的标题']
        )
        ->addColumn(
            'meta_icon',
            'string',
            ['limit' => 32, 'null' => true, 'default' => '', 'comment' => '展示的图标']
        )
        ->addTimestamps()
        ->create();
    }
}
