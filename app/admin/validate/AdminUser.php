<?php

namespace app\admin\validate;

use think\Validate;
use app\admin\model\AdminRole as RoleModel;
use app\admin\model\AdminUser as UserModel;

class AdminUser extends Validate
{
    protected $rule = [
        'name' => 'require|min:3|max:25',
        'password' => 'require|min:6|max:18',
    ];

    protected $message = [
        'username.unique' => '用户名已存在',
        'name.require' => '用户名不能为空',
        'name.min' => '用户名最少为3个字符',
        'name.max' => '用户名最多为25个字符',
        'password.require' => '密码不能为空',
        'password.min' => '密码最少为6位',
        'password.max' => '密码最多为18位'
    ];

    public function sceneInsert()
    {
        return $this->append('admin_role_id', 'checkAdminRoleId')
            ->append('username', 'unique:admin_user,username');
    }

    public function sceneUpdate()
    {
        return $this->append('admin_role_id', 'checkAdminRoleId')
            ->append('username', 'unique:admin_user,username');
    }

    public function checkAdminRoleId($value, $rule)
    {
        if (!RoleModel::field('id')->find($value)) {
            return '角色不存在';
        }
        return true;
    }
}
