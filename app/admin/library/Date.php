<?php
namespace app\admin\library;

class Date
{
    public static function getLease($checkin, $i = 1)
    {
        $star = date('Y-m-d', strtotime("$checkin +$i month"));
        $end = date('Y-m-d', strtotime("$checkin +$i month - 1 day +1 month"));
        return array($star,$end);
    }
}
