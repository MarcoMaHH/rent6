<?php

namespace app\admin\library;

use app\admin\library\Tree;

class Menu extends Tree
{
    public function getTree($curr = '')
    {
        $data = $this->data;
        foreach ($data as $k => $v) {
            if ($data[$k]['meta_icon']) {
                $data[$k]['meta']['icon'] = $data[$k]['meta_icon'];
            }
            $data[$k]['meta']['title'] = $data[$k]['meta_title'];
        }
        return $this->tree($data, 0);
    }
    protected function isCurr($test, $curr)
    {
        return ($test === $curr) || ($test . '.' === substr($curr, 0, strlen($test) + 1));
    }
    public function getCurrentId($curr = '')
    {
        $result = 0;
        $data = $this->data;
        foreach ($data as $k => $v) {
            if ($v['controller'] === $curr) {
                $result = $v['id'];
                break;
            }
        }
        return $result;
    }
}
