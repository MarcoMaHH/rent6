<?php

namespace app\admin\controller;

use app\admin\library\Auth;
use app\BaseController;

//账单类型-租金
define('TYPE_RENT', 'A');
//账单类型-电费
define('TYPE_ELECTRICITY', 'B');
//账单类型-水费
define('TYPE_WATER', 'C');
//账单类型-维修费
define('TYPE_FIX', 'D');
//账单类型-工资
define('TYPE_SALARY', 'E');
//账单类型-其他
define('TYPE_OTHER', 'F');

class Common extends BaseController
{
    protected $auth;
    protected $user;
    protected $checkLoginExclude = [];

    protected function initialize()
    {
        ini_set('session.use_cookies', 0);
        $this->auth = Auth::getInstance();
        $action = $this->request->action();
        $controller = $this->request->controller();
        if (in_array($action, $this->checkLoginExclude)) {
            return;
        }
        if (!$this->auth->isLogin()) {
            throw new \think\Exception('你还没登陆。', 995);
        }
        if (!$this->auth->checkAuth($controller, $action)) {
            throw new \think\Exception('你没有该权限。', 995);
        }
        $this->user = $this->auth->getLoginUser();
    }

    public function returnWeb($data = [])
    {
        $result = [
            "code" => 0,
            "data" => ['list' => $data]
        ];
        return \json($result);
    }

    public function returnError($msg = '系统出错')
    {
        $data = [
            "code" => 995,
            "message" =>  $msg
        ];
        return \json($data);
    }

    public function returnSuccess($msg = '操作成功')
    {
        $data = [
            "code" => 0,
            "message" =>  $msg
        ];
        return \json($data);
    }



    // todo
    public function returnWechat($data = [], $count = 0, $msg = '')
    {
        if (!$count) {
            $count = \count($data);
        }
        $data = [
            "code" => 1,
            "msg" =>  $msg,
            "count" => $count,
            "data" => $data
        ];
        return \json($data);
    }
}
