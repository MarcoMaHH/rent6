<?php

namespace app\admin\controller;

use app\admin\model\HouseProperty as PropertyModel;
use app\admin\model\HouseBilling as BillingModel;
use app\admin\model\BillSum as SumModel;
use app\admin\model\AdminUser as UserModel;
use app\admin\validate\AdminUser as UserValidate;
use think\facade\Request;

class Index extends Common
{
    protected $checkLoginExclude = ['login', 'menu'];

    public function login()
    {
        $data = [
            'name' => $this->request->post('account/s', '', 'trim'),
            'password' => $this->request->post('password/s', '')
        ];
        $validate = new UserValidate();
        if (!$validate->check($data)) {
            return $this->returnError($validate->getError());
        }
        $result = $this->auth->login($data['name'], $data['password']);
        if (!$result) {
            return $this->returnError($this->auth->getError());
        }
        return \json(["code" => 0, "data" => $result]);
    }

    public function menu()
    {
        $controller = $this->request->controller();
        return $this->returnWeb($this->auth->menu($controller));
    }

    public function dashboard()
    {
        $loginUser = $this->auth->getLoginUser();
        $property_count = PropertyModel::where('admin_user_id', $loginUser['id'])->count();
        $user = UserModel::find($loginUser['id']);
        $number_count =  $user->houseNumber->count();
        $empty_count =  $user->houseNumber->where('rent_mark', 'N')->count();
        $occupancy = $number_count == 0 ? '0%' : round((($number_count - $empty_count) / $number_count) * 100) . '%';
        $property = PropertyModel::where('admin_user_id', $loginUser['id'])->select()->toArray();
        $result = array_map(function ($item) {
            return $item['id'];
        }, $property);
        $star = date("Y-m-01", strtotime("first day of -0 Months"));
        $end = date('Y-m-d', strtotime("$star +1 month -1 day"));
        $income = SumModel::where('house_property_id', 'in', $result)
        ->whereTime('accounting_date', 'between', [$star, $end])
        ->where('type', 'A')
        ->sum('total_money');
        $spending = SumModel::where('house_property_id', 'in', $result)
        ->whereTime('accounting_date', 'between', [$star, $end])
        ->where('type', 'in', ['B', 'C', 'D', 'E', 'F'])
        ->sum('total_money');
        $house_info = [
            ['title' => '本月利润','number' => $income - intval($spending),],
            ['title' => '入住率','number' => $occupancy,],
            ['title' => '闲置房间数','number' => $empty_count,],
            ['title' => '总房间数','number' => $number_count,],
        ];
        return $this->returnWeb($house_info);
    }

    public function echar()
    {
        $loginUser = $this->auth->getLoginUser();
        $property = PropertyModel::where('admin_user_id', $loginUser['id'])->select()->toArray();
        $result = array_map(function ($item) {
            return $item['id'];
        }, $property);
        $charData = array();
        for ($i = 11; $i >= 0; $i--) {
            $star = date("Y-m-01", strtotime("first day of -$i Months"));
            $end = date('Y-m-d', strtotime("$star +1 month -1 day"));
            $income = SumModel::where('house_property_id', 'in', $result)
            ->whereTime('accounting_date', 'between', [$star, $end])
            ->where('type', 'A')
            ->sum('total_money');
            $spending = SumModel::where('house_property_id', 'in', $result)
            ->whereTime('accounting_date', 'between', [$star, $end])
            ->where('type', 'in', ['B', 'C', 'D', 'E', 'F'])
            ->sum('total_money');
            \array_push($charData, ['month' => \substr($star, 0, 7), 'project' => '收入', 'money' => $income]);
            \array_push($charData, ['month' => \substr($star, 0, 7), 'project' => '支出', 'money' => intval($spending)]);
            \array_push($charData, ['month' => \substr($star, 0, 7), 'project' => '利润', 'money' => $income - intval($spending)]);
        }
        return $this->returnElement($charData);
    }
}
