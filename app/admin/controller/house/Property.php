<?php

namespace app\admin\controller\house;

use app\admin\controller\Common;
use app\admin\library\Property as PropertyLib;
use app\admin\validate\HouseProperty as PropertyValidate;
use app\admin\model\HouseProperty as PropertyModel;

class Property extends Common
{
    public function query()
    {
        $loginUser = $this->auth->getLoginUser();
        $property = PropertyModel::where('admin_user_id', $loginUser['id'])
        ->order('id')
        ->select();
        return $this->returnWeb($property);
    }

    public function save()
    {
        $id = $this->request->post('id/d', 0);
        $data = [
            'name' => $this->request->post('name/s', '', 'trim'),
            'address' => $this->request->post('address/s', '', 'trim'),
        ];
        if ($id) {
            if (!$property = PropertyModel::find($id)) {
                return $this->returnError('修改失败，记录不存在。');
            }
            $property->save($data);
            return $this->returnSuccess('修改成功');
        }
        $loginUser = $this->auth->getLoginUser();
        $data['admin_user_id'] = $loginUser['id'];
        $result = PropertyModel::create($data);
        return $this->returnSuccess('添加成功');
    }

    public function delete()
    {
        $id = $this->request->param('id/d', 0);
        $validate = new PropertyValidate();
        if (!$validate->scene('delete')->check(['id' => $id])) {
            return $this->returnError('删除失败，' . $validate->getError() . '。');
        }
        if (!$property = PropertyModel::find($id)) {
            return $this->returnError('删除失败，记录不存在。');
        }
        $property->delete();
        return $this->returnSuccess('删除成功');
    }

    public function sort()
    {
        $id = $this->request->post('id/d', 0);
        $loginUser = $this->auth->getLoginUser();
        $data = PropertyModel::where('admin_user_id', $loginUser['id'])->select()->toArray();
        $result = [];
        foreach ($data as $value) {
            $temp = array(
                'id' => $value['id']
            );
            if ($value['id'] === $id) {
                $temp['firstly'] = 'Y';
            } else {
                $temp['firstly'] = 'N';
            }
            \array_push($result, $temp);
        }
        $property = new PropertyModel();
        $property->saveAll($result);
        return $this->returnSuccess('切换成功');
    }
}
