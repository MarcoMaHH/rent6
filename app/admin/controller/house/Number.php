<?php

namespace app\admin\controller\house;

use app\admin\controller\Common;
use app\admin\model\HouseProperty as PropertyModel;
use app\admin\model\HouseNumber as NumberModel;
use app\admin\validate\HouseNumber as NumberValidate;
use app\admin\model\HouseTenant as TenantModel;
use app\admin\model\HouseBilling as BillingModel;
use app\admin\library\Property;
use app\admin\library\Date;
use think\facade\Db;

class Number extends Common
{
    public function query()
    {
        $loginUser = $this->auth->getLoginUser();
        $house_property_id = $this->request->param('house_property_id/d', Property::getProperty($loginUser['id']));
        $numbers = NumberModel::where('a.house_property_id', $house_property_id)
        ->alias('a')
        ->join('HouseProperty b', 'a.house_property_id = b.id')
        ->field('a.*,b.name as property_name')
        ->order('a.name')
        ->select();
        foreach ($numbers as  $value) {
            if ($value['lease']) {
                $value['rent_date'] = Date::getLease($value['checkin_time'], $value['lease'] - 1)[0];
            }
        }
        return $this->returnWeb($numbers);
    }

    public function save()
    {
        $id = $this->request->post('id/d', 0);
        $data = [
            'house_property_id' => $this->request->post('propertyID/d', 0),
            'name' => $this->request->post('name/s', '', 'trim'),
            'rental' => $this->request->post('rental/d', 0),
            'deposit' => $this->request->post('deposit/d', 0),
            'management' => $this->request->post('management/d', 0),
            'daily_rent' => $this->request->post('daily_rent/d', 0),
            'water_price' => $this->request->post('water_price/f', 0.0),
            'electricity_price' => $this->request->post('electricity_price/f', 0.0),
        ];
        $validate = new NumberValidate();
        if ($id) {
            if (!$validate->scene('update')->check($data)) {
                return $this->returnError('修改失败，' . $validate->getError() . '。');
            }
            if (!$permission = NumberModel::find($id)) {
                return $this->returnError('修改失败，记录不存在。');
            }
            $permission->save($data);
            return $this->returnSuccess('修改成功');
        }
        if (!$validate->scene('insert')->check($data)) {
            return $this->returnError('添加失败，' . $validate->getError() . '。');
        }
        $result = NumberModel::create($data);
        return $this->returnSuccess('添加成功');
    }

    //新租
    public function checkin()
    {
        // 租客资料
        $house_number_id = $this->request->post('number_id/d', 0);
        $checkin_time = $this->request->post('checkin_time/s', '', 'trim');
        $data = [
            'house_property_id' => $this->request->post('property_id/d', 0),
            'house_number_id' => $house_number_id,
            'name' => $this->request->post('tenant_name/s', '', 'trim'),
            'sex' => $this->request->post('sex/s', '', 'trim'),
            'checkin_time' => $checkin_time,
            'phone' => $this->request->post('phone/d', ''),
            'id_card_number' => $this->request->post('id_card_number/d', ''),
            'native_place' => $this->request->post('native_place/s', '', 'trim'),
            'work_units' => $this->request->post('work_units/s', '', 'trim'),
        ];
        if (!$number_data = NumberModel::find($house_number_id)) {
            $this->returnError('修改失败，记录不存在');
        }
        // 账单资料
        $note = "单据开出中途退房，一律不退房租。 \n" .
                "到期如果不续租，超期将按每天" . $number_data['daily_rent'] . "元计算。" ;
        $transFlag = true;
        Db::startTrans();
        try {
            //insert租客资料
            $tenant = TenantModel::create($data);
            //insert账单资料
            $billing_data = [
                'house_property_id' => $data['house_property_id'],
                'house_number_id' => $data['house_number_id'],
                'start_time' => $checkin_time,
                'end_time' => date('Y-m-d', strtotime("$checkin_time +1 month -1 day")),
                'tenant_id' => $tenant->id,
                'rental' => $number_data['rental'],
                'deposit' => $number_data['deposit'],
                'total_money' => $number_data['deposit'] + $number_data['rental'],
                'note' => $note
            ];
            $billing = BillingModel::create($billing_data);
            //update房号资料
            $update_data = [
                'tenant_id' => $tenant->id,
                'receipt_number' => $billing->id,
                'payment_time' => $checkin_time,
                'checkin_time' => $checkin_time,
                'rent_mark' => 'Y',
                'lease' => 1,
            ];
            $number_data->save($update_data);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $transFlag = false;
            // 回滚事务
            Db::rollback();
        }
        if ($transFlag) {
            return $this->returnSuccess('添加租客成功');
        }
    }

    //退房
    public function checkout()
    {
        $number_id = $this->request->param('id/d', 0);
        $leave_time = $this->request->param('leave_time/s', date('Y-m-d'), 'trim');
        if (!$number_data = NumberModel::find($number_id)) {
            $this->returnError('修改失败，记录不存在');
        }
        $number_update = [
            'rent_mark' => 'N',
            'tenant_id' => '',
            'checkin_time' => null,
            // 'payment_time' => date('Y-m-d'),
            'lease' => 0,
        ];
        $billing_data = BillingModel::find($number_data->receipt_number);
        $datediff = intval((strtotime($leave_time) - strtotime($billing_data->start_time)) / (60 * 60 * 24));
        $note = '';
        $rental = 0;
        if ($datediff > 0) {
            $rental = $datediff * $number_data->daily_rent;
            $note = '租金为' . $datediff . '*' . $number_data->daily_rent . '=' . $rental . '。';
        }
        $billing_update = [
            'start_time' => $leave_time,
            'meter_reading_time' => $leave_time,
            'end_time' => null,
            'rental' => $rental,
            'deposit' => 0 - $number_data->deposit,
            'garbage_fee' => 0,
            'note' => $note,
        ];
        $transFlag = true;
        Db::startTrans();
        try {
            $number_data->save($number_update);
            TenantModel::where('house_property_id', $number_data->house_property_id)
            ->where('house_number_id', $number_id)
            ->where('leave_time', 'null')
            ->data(['leave_time' => $leave_time, 'mark' => 'Y'])
            ->update();
            $billing_data->save($billing_update);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $transFlag = false;
            // 回滚事务
            Db::rollback();
        }
        if ($transFlag) {
            return $this->returnSuccess('退房成功');
        }
    }

    public function delete()
    {
        $id = $this->request->param('id/d', 0);
        if (!$numberArr = NumberModel::find($id)) {
            return $this->returnError('修改失败，记录不存在。');
        }
        $numberArr->delete();
        return $this->returnSuccess('删除成功');
    }
}
