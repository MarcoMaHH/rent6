<?php

namespace app\api\controller;

use app\admin\library\Property;
use app\admin\model\HouseProperty as PropertyModel;
use app\admin\model\BillSum as SumModel;
use app\admin\model\AdminUser as UserModel;

class Report extends Common
{
    public function query()
    {
        $loginUser = $this->auth->getLoginUser();
        $house_property_id = $this->request->param('house_property_id/d', Property::getProperty($loginUser['id']));
        $account1 = SumModel::where('house_property_id', $house_property_id)
        ->whereTime('accounting_date', 'today')
        ->where('type', '=', 'A')
        ->sum('total_money');
        $account2 = SumModel::where('house_property_id', $house_property_id)
        ->where('type', '<>', 'A')
        ->whereTime('accounting_date', 'today')
        ->sum('total_money');
        $user = UserModel::find($loginUser['id']);
        $number_count =  $user->houseNumber->where('house_property_id', $house_property_id)->count();
        $empty_count =  $user->houseNumber->where('rent_mark', 'N')->where('house_property_id', $house_property_id)->count();
        $occupancy = $number_count == 0 ? '0%' : round((($number_count - $empty_count)/$number_count)*100).'%';
        $house_info = [
            'account' => $account1 - intval($account2),
            'number_count' => $number_count,
            'empty_count' => $empty_count,
            'occupancy' => $occupancy,
            'overdue_todo' => $user->houseBilling->where('accounting_date', null)
                ->where('meter_reading_time', '=', '')
                ->where('house_property_id', $house_property_id)
                ->where('start_time', '<', date('Y-m-d H:i:s', time()))
                ->count(),
            'overdue_uncollected' => $user->houseBilling->where('accounting_date', null)
                ->where('meter_reading_time', '!=', '')
                ->where('house_property_id', $house_property_id)
                ->where('start_time', '<', date('Y-m-d H:i:s', time()))
                ->count(),
        ];
        return $this->returnWechat($house_info);
    }
}
