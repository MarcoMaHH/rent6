# 最常用出租屋管理系统-rent6

自家用的出租屋管理系统。本系统基于Thinphp8(PHP8) + TDesign Starter(vue3，TS)开发，前后端分离。

[最常用出租屋管理系统rent6-前端](https://gitee.com/MarcoMaHH/rent6_web)

[最常用出租屋管理系统rent6-后端](https://gitee.com/MarcoMaHH/rent6)

---

本人的微信小程序项目，欢迎大家扫码体验！！！

![最常用摸鱼助手](https://gitee.com/MarcoMaHH/picture/raw/master/project.jpg)

---

**其他版本**：

[最常用出租屋管理系统rent2混编版-thinkph5.1+layUi](https://gitee.com/MarcoMaHH/rent2)

[最常用出租屋管理系统rent4混编版-thinkphp6+elementUI](https://gitee.com/MarcoMaHH/rent4)

[最常用出租屋管理系统rent6前后端分离版-thinkphp8+TDesign](https://gitee.com/MarcoMaHH/rent6)

[最常用出租屋管理系统rent8混编版-thinkphp8+TDesign](https://gitee.com/MarcoMaHH/rent8)

### 系统环境

- PHP = 8.1.22

- Apache = 2.4.41

- MySQL = 5.7.28

### 界面及功能展示

登录页面

![登录页面](https://gitee.com/MarcoMaHH/rent6/raw/master/picture/login.jpg)

首页

![首页](https://gitee.com/MarcoMaHH/rent6/raw/master/picture/dashboard.jpg)

“房产管理”页面

![房产管理页面](https://gitee.com/MarcoMaHH/rent6/raw/master/picture/property.jpg)

“房间管理”页面

![房间管理页面](https://gitee.com/MarcoMaHH/rent6/raw/master/picture/number.jpg)

“未收账单”页面

![未收账单页面](https://gitee.com/MarcoMaHH/rent6/raw/master/picture/uncollected.jpg)

“未收账单”页面-收据单

![收据单](https://gitee.com/MarcoMaHH/rent6/raw/master/picture/receipt.jpg)

“租客管理”页面

![租客管理页面](https://gitee.com/MarcoMaHH/rent6/raw/master/picture/tenant.jpg)

### 安装步骤

1. 建立数据库`zcy`
2. `git clone https://gitee.com/MarcoMaHH/rent6.git`
3. 将.example.env改为.env，并修改为自己的数据
4. 在根目录执行`composer install`
5. `php think migrate:run`
6. `php think seed:run`

### 原始账号密码

账号：admin  密码：123456

账号：user     密码：123456
